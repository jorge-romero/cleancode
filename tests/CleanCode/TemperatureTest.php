<?php

namespace Tests\CleanCode;

use CleanCode\Exceptions\TemperatureNegativeException;
use CleanCode\Interfaces\IGetMeasureFromDB;
use CleanCode\Temperature;
use CleanCode\TemperatureTestClass;
use PHPUnit\Framework\TestCase;

class TemperatureTest extends TestCase implements IGetMeasureFromDB
{
    public function validTemperatureDataProvider()
    {
        return [
            [-1, TRUE],
            [0, FALSE],
            [1, FALSE],
        ];
    }

    /**
     * @dataProvider validTemperatureDataProvider
     */
    public function testValidTemperature($measure, $expectException)
    {
        if ($expectException) {
            $this->expectException(TemperatureNegativeException::class);
        }

        $this->assertSame($measure, Temperature::take($measure)->measure());
    }
    /*
    public function testTryToCreateAValidTemperatureWithNamedConstruct()
    {
        $this->expectException(TemperatureNegativeException::class);

        Temperature::take(-1);
    }

    public function testTryToCreateANonValidTemperature()
    {
        $this->expectException(TemperatureNegativeException::class);

        Temperature::take(-1);
    }*/

    public function testTryToCreateAValidTemperature()
    {
        $measure = 18;

        $temperature = Temperature::take($measure);

        $this->assertSame($measure, $temperature->measure());
    }
    
    public function temperatureIsDangerousDataProvider()
    {
        return [
            [FALSE, 79],
            [FALSE, 80],
            [TRUE, 81],
        ];
    }

    /**
     * @dataProvider temperatureIsDangerousDataProvider
     */
    public function testTemperatureIsDangerous($expected, $measure)
    {
        $temperature = TemperatureTestClass::take($measure);

        $this->assertSame($expected, $temperature->isDangerous());
    }

    /*
    public function testCheckTemperatureIfColdTemperatureIsDangerous()
    {
        $measure = 10;

        $temperature = TemperatureTestClass::take($measure);

        $this->assertFalse($temperature->isDangerous());
    }
    
    public function testCheckTemperatureIfHotTemperatureIsDangerous()
    {
        $measure = 85;

        $temperature = TemperatureTestClass::take($measure);

        $this->assertTrue($temperature->isDangerous());
    }*/
    
    public function temperatureIsNormalDataProvider()
    {
        return [
            [TRUE, 39],
            [FALSE, 40],
            [FALSE, 41],
        ];
    }

    /**
     * @dataProvider temperatureIsNormalDataProvider
     */
    public function testTemperatureIsNormal($expected, $measure)
    {
        $temperature = Temperature::take($measure);

        $this->assertSame($expected, $temperature->isNormal($this));
    }

    public function testCheckTemperatureIfColdTemperatureIsNormal()
    {
        $measure = 10;

        $temperature = Temperature::take($measure);

        $this->assertTrue($temperature->isNormal($this));
    }

    public function getNormalLimitFromDB() 
    {
        return 40;
    }

    public function testCheckTemperatureIfColdTemperatureIsNormalWithAnonymousClass()
    {
        $measure = 10;

        $temperature = Temperature::take($measure);

        $this->assertTrue($temperature->isNormal(
            new class implements IGetMeasureFromDB
            {
                public function getNormalLimitFromDB()
                {
                    return 40;
                }
            }
        ));
    }

    public function testTryToCreateTemperatureFromStation()
    {
        $expectedMeasure = 80;

        $measureFromStation = Temperature::fromStation($this)->measure();

        $this->assertSame($expectedMeasure, $measureFromStation);
    }

    public function sensor() 
    {
        return $this;
    }

    public function Temperature() 
    {
        return $this;
    }
    
    public function measure() 
    {
        return 80;
    }

    public function testTryToCreateTemperatureFromStationAnonymousClass()
    {
        $expectedMeasure = 80;

        $measureFromStation = Temperature::fromStation(
            new class
            {
                public function sensor() 
                {
                    return $this;
                }

                public function Temperature() 
                {
                    return $this;
                }
                
                public function measure() 
                {
                    return 80;
                }                
            }
        )->measure();

        $this->assertSame($expectedMeasure, $measureFromStation);
    }

    public function testAddTemperature()
    {
        $measureA = 25;
        $measureB = 45;
        $temperatureA = Temperature::take($measureA);
        $temperatureB = Temperature::take($measureB);

        $temperatureTotal = $temperatureA->add($temperatureB);

        $temperatureTotalMeasure = $temperatureTotal->measure();

        $this->assertSame(70, $temperatureTotalMeasure);
        $this->assertSame($measureA, $temperatureA->measure());
        $this->assertSame($measureB, $temperatureB->measure());
    }
}
