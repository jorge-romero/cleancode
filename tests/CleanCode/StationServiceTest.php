<?php

namespace Tests\CleanCode;

use CleanCode\Exceptions\TemperatureHighException;
use CleanCode\Interfaces\IGetMeasureFromDB;
use CleanCode\Interfaces\INotification;
use CleanCode\Services\StationService;
use CleanCode\Types\Notifications\EmailNotification;
use PHPUnit\Framework\TestCase;

class StationServiceTest extends TestCase
{
    public function testCheckTemperature()
    {
        $notificationType = EmailNotification::create();
        $stationService = StationService::create();

        $getMeasureFromDBClass = new class implements IGetMeasureFromDB
        {
            public function getNormalLimitFromDB()
            {
                return 40;
            }
        };

        $response = $stationService->checkTemperature($getMeasureFromDBClass, $notificationType);

        $this->assertTrue($response);
    }

    public function testCheckTemperatureHighException()
    {
        $this->expectException(TemperatureHighException::class);

        $notificationType = EmailNotification::create();
        $stationService = StationService::create();

        $getMeasureFromDBClass = new class implements IGetMeasureFromDB
        {
            public function getNormalLimitFromDB()
            {
                return 81;
            }
        };

        $response = $stationService->checkTemperature($getMeasureFromDBClass, $notificationType);
    }
}
