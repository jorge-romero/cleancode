<?php

namespace Tests\Solid\OpenClose;

use PHPUnit\Framework\TestCase;
use Solid\OpenClose\Customer;

class CustomerTest extends TestCase
{
    public function testSetNormalPrice()
    {
        $normalPrice = random_int(1, 1000);

        $customer = new Customer();

        $customer->setNormalPrice($normalPrice);

        $this->assertEquals($normalPrice, $customer->getNormalPrice());
    }

    public function testGetMembershipPriceDefault()
    {
        $normalPrice = random_int(1, 1000);

        $customer = new Customer();

        $customer->setNormalPrice($normalPrice);

        $this->assertEquals($normalPrice, $customer->getMembershipPrice());
    }

    public function testGetMembershipPriceToExternalType()
    {
        $normalPrice = random_int(1, 1000);

        $customer = new Customer();

        $customer->setNormalPrice($normalPrice);

        $expected = ($normalPrice + ($normalPrice * 0.2));

        $this->assertEquals($expected, 
            $customer->getMembershipPrice(Customer::EXTERNAL));
    }

    public function testGetMembershipPriceToInternalType()
    {
        $normalPrice = random_int(1, 1000);

        $customer = new Customer();

        $customer->setNormalPrice($normalPrice);

        $this->assertEquals($normalPrice, 
            $customer->getMembershipPrice(Customer::INTERNAL));
    }

    public function testGetMembershipPriceToPreferenceType()
    {
        $normalPrice = random_int(1, 1000);

        $customer = new Customer();

        $customer->setNormalPrice($normalPrice);

        $expected = ($normalPrice + ($normalPrice * -0.1));

        $this->assertEquals($expected, 
            $customer->getMembershipPrice(Customer::PREFERENCE));
    }
}
