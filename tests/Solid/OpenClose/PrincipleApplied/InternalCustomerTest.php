<?php

namespace Tests\Solid\OpenClose\PrincipleApplied;

use PHPUnit\Framework\TestCase;
use Solid\OpenClose\PrincipleApplied\InternalCustomer;

class InternalCustomerTest extends TestCase
{
    public function testGetMembershipPrice()
    {
        $normalPrice = random_int(1, 1000);

        $internalCustomer = new InternalCustomer();

        $internalCustomer->setNormalPrice($normalPrice);

        $this->assertEquals($normalPrice, $internalCustomer->getMembershipPrice());
    }
}
