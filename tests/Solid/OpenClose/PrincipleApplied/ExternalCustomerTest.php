<?php

namespace Tests\Solid\OpenClose\PrincipleApplied;

use PHPUnit\Framework\TestCase;
use Solid\OpenClose\PrincipleApplied\ExternalCustomer;

class ExternalCustomerTest extends TestCase
{
    public function testGetMembershipPrice()
    {
        $normalPrice = random_int(1, 1000);

        $externalCustomer = new ExternalCustomer();

        $externalCustomer->setNormalPrice($normalPrice);

        $expected = ($normalPrice + ($normalPrice * 0.2));

        $this->assertEquals($expected, $externalCustomer->getMembershipPrice());
    }
}
