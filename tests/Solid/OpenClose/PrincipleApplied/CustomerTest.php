<?php

namespace Tests\Solid\OpenClose\PrincipleApplied;

use PHPUnit\Framework\TestCase;
use Solid\OpenClose\PrincipleApplied\Customer;

class CustomerTest extends TestCase
{
    public function testSetNormalPrice()
    {
        $normalPrice = random_int(1, 1000);

        $customer = new Customer();

        $customer->setNormalPrice($normalPrice);

        $this->assertEquals($normalPrice, $customer->getNormalPrice());
    }

    public function testGetMembershipPrice()
    {
        $normalPrice = random_int(1, 1000);

        $customer = new Customer();

        $customer->setNormalPrice($normalPrice);

        $this->assertEquals($normalPrice, $customer->getMembershipPrice());
    }
}
