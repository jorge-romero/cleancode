<?php

namespace Tests\Solid\OpenClose\PrincipleApplied;

use PHPUnit\Framework\TestCase;
use Solid\OpenClose\PrincipleApplied\PreferenceCustomer;

class PreferenceCustomerTest extends TestCase
{
    public function testGetMembershipPrice()
    {
        $normalPrice = random_int(1, 1000);

        $preferenceCustomer = new PreferenceCustomer();

        $preferenceCustomer->setNormalPrice($normalPrice);

        $expected = ($normalPrice + ($normalPrice * -0.1));

        $this->assertEquals($expected, $preferenceCustomer->getMembershipPrice());
    }
}
