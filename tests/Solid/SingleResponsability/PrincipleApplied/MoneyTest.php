<?php

namespace Tests\Solid\SingleResponsability\principleApplied;

use PHPUnit\Framework\TestCase;
use Solid\SingleResponsability\principleApplied\Money;

class MoneyTest extends TestCase
{
    public function testMoney()
    {
        $currency = '$';
        $value = random_int(1, 1000);

        $money = new Money($currency, $value);

        $this->assertEquals($currency, $money->getCurrency());
        $this->assertEquals($value, $money->getValue());
    }
}
