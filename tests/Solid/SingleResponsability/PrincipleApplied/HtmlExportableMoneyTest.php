<?php

namespace Tests\Solid\SingleResponsability\principleApplied;

use PHPUnit\Framework\TestCase;
use Solid\SingleResponsability\principleApplied\ExportableMoneyInterface;
use Solid\SingleResponsability\principleApplied\HtmlExportableMoney;
use Solid\SingleResponsability\principleApplied\Money;

class HtmlExportableMoneyTest extends TestCase
{
    public function testIsInstanceOfExportableMoneyInterface()
    {
        $htmlExportableMoney = new HtmlExportableMoney();

        $this->assertInstanceOf(ExportableMoneyInterface::class, $htmlExportableMoney);
    }

    public function testExport()
    {
        $currency = '$';
        $value = random_int(1, 1000);

        $money = new Money($currency, $value);

        $htmlExportableMoney = new HtmlExportableMoney();
        $expected = "<span><b>{$currency}</b>&nbsp;{$value}</span>";
        
        $this->assertEquals($expected, $htmlExportableMoney->export($money));
    }
}
