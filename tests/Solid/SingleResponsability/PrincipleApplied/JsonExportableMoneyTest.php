<?php

namespace Tests\Solid\SingleResponsability\principleApplied;

use PHPUnit\Framework\TestCase;
use Solid\SingleResponsability\principleApplied\ExportableMoneyInterface;
use Solid\SingleResponsability\principleApplied\JsonExportableMoney;
use Solid\SingleResponsability\principleApplied\Money;

class JsonExportableMoneyTest extends TestCase
{
    public function testIsInstanceOfExportableMoneyInterface()
    {
        $jsonExportableMoney = new JsonExportableMoney();

        $this->assertInstanceOf(ExportableMoneyInterface::class, $jsonExportableMoney);
    }

    public function testExport()
    {
        $currency = '$';
        $value = random_int(1, 1000);

        $money = new Money($currency, $value);

        $jsonExportableMoney = new JsonExportableMoney();

        $data = [
            'currency' => $money->getCurrency(),
            'value' => $money->getValue(),
        ];
        $expected = json_encode($data);
        
        $this->assertEquals($expected, $jsonExportableMoney->export($money));
    }
}
