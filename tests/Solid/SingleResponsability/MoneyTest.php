<?php

namespace Tests\Solid\SingleResponsability;

use PHPUnit\Framework\TestCase;
use Solid\SingleResponsability\Money;

class MoneyTest extends TestCase
{
    public function testMoney()
    {
        $currency = '$';
        $value = random_int(1, 1000);

        $money = new Money($currency, $value);

        $this->assertEquals($currency, $money->getCurrency());
        $this->assertEquals($value, $money->getValue());
    }

    public function testToHTML()
    {
        $currency = '$';
        $value = random_int(1, 1000);

        $money = new Money($currency, $value);

        $expected = "<span><b>{$currency}</b>&nbsp;{$value}</span>";
        $this->assertEquals($expected, $money->toHTML());
    }

    public function testToJson()
    {
        $currency = '$';
        $value = random_int(1, 1000);

        $money = new Money($currency, $value);

        $data = [
            'currency' => $currency,
            'value' => $value,
        ];

        $expected = json_encode($data);

        $this->assertEquals($expected, $money->toJson());
    }
}
