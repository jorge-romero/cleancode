<?php

namespace Tests\Solid\LiskovSubstitution\PrincipleApplied;

use PHPUnit\Framework\TestCase;
use Solid\LiskovSubstitution\PrincipleApplied\Bird;
use Solid\LiskovSubstitution\PrincipleApplied\Ostrich;

class OstrichTest extends TestCase
{
    public function testIsInstanceOfBird()
    {
        $ostrich = new Ostrich();

        $this->assertInstanceOf(Bird::class, $ostrich);
    }

    public function testOstrich()
    {
        $ostrich = new Ostrich();

        $this->assertEquals('insects, plants', $ostrich->eat());
        $this->assertEquals('chirp', $ostrich->tweet());
        $this->assertEquals('run', $ostrich->move());
    }
}
