<?php

namespace Tests\Solid\LiskovSubstitution\PrincipleApplied;

use PHPUnit\Framework\TestCase;
use Solid\LiskovSubstitution\PrincipleApplied\AnimalFoundation;
use Solid\LiskovSubstitution\PrincipleApplied\Bird;
use Solid\LiskovSubstitution\PrincipleApplied\Duck;
use Solid\LiskovSubstitution\PrincipleApplied\Ostrich;
use Solid\LiskovSubstitution\PrincipleApplied\Penguin;
use Solid\LiskovSubstitution\PrincipleApplied\Sparrow;

class AnimalFoundationTest extends TestCase
{
    public function testReleaseBirds()
    {
        $animalFoundation = new AnimalFoundation();

        $birds = [
            new Bird(),
            new Duck(),
            new Ostrich(),
            new Penguin(),
            new Sparrow(),
        ];

        $this->assertTrue($animalFoundation->releaseBirds($birds));
    }
}
