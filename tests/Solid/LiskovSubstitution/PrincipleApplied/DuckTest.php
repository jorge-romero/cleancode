<?php

namespace Tests\Solid\LiskovSubstitution\PrincipleApplied;

use PHPUnit\Framework\TestCase;
use Solid\LiskovSubstitution\PrincipleApplied\Bird;
use Solid\LiskovSubstitution\PrincipleApplied\Duck;

class DuckTest extends TestCase
{
    public function testIsInstanceOfBird()
    {
        $duck = new Duck();

        $this->assertInstanceOf(Bird::class, $duck);
    }

    public function testDuck()
    {
        $duck = new Duck();

        $this->assertEquals('fish', $duck->eat());
        $this->assertEquals('quack', $duck->tweet());
        $this->assertEquals('fly', $duck->move());
    }
}
