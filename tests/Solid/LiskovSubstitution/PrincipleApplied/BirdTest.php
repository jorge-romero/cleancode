<?php

namespace Tests\Solid\LiskovSubstitution\PrincipleApplied;

use PHPUnit\Framework\TestCase;
use Solid\LiskovSubstitution\PrincipleApplied\Bird;

class BirdTest extends TestCase
{
    public function testBird()
    {
        $bird = new Bird();

        $this->assertEquals('eat', $bird->eat());
        $this->assertEquals('tweet', $bird->tweet());
        $this->assertEquals('move', $bird->move());
    }
}
