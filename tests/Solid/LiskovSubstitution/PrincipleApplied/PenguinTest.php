<?php

namespace Tests\Solid\LiskovSubstitution\PrincipleApplied;

use PHPUnit\Framework\TestCase;
use Solid\LiskovSubstitution\PrincipleApplied\Bird;
use Solid\LiskovSubstitution\PrincipleApplied\Penguin;

class PenguinTest extends TestCase
{
    public function testIsInstanceOfBird()
    {
        $penguin = new Penguin();

        $this->assertInstanceOf(Bird::class, $penguin);
    }

    public function testDuck()
    {
        $penguin = new Penguin();

        $this->assertEquals('fish', $penguin->eat());
        $this->assertEquals('squawk', $penguin->tweet());
        $this->assertEquals('swim', $penguin->move());
    }
}
