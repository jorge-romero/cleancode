<?php

namespace Tests\Solid\LiskovSubstitution\PrincipleApplied;

use PHPUnit\Framework\TestCase;
use Solid\LiskovSubstitution\PrincipleApplied\Bird;
use Solid\LiskovSubstitution\PrincipleApplied\Sparrow;

class SparrowTest extends TestCase
{
    public function testIsInstanceOfBird()
    {
        $sparrow = new Sparrow();

        $this->assertInstanceOf(Bird::class, $sparrow);
    }

    public function testSparrow()
    {
        $sparrow = new Sparrow();

        $this->assertEquals('insect', $sparrow->eat());
        $this->assertEquals('chirp', $sparrow->tweet());
        $this->assertEquals('fly', $sparrow->move());
    }
}
