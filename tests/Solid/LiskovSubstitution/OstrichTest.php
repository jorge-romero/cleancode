<?php

namespace Tests\Solid\LiskovSubstitution;

use PHPUnit\Framework\TestCase;
use Solid\LiskovSubstitution\Bird;
use Solid\LiskovSubstitution\Ostrich;

class OstrichTest extends TestCase
{
    public function testIsInstanceOfBird()
    {
        $ostrich = new Ostrich();

        $this->assertInstanceOf(Bird::class, $ostrich);
    }

    public function testOstrich()
    {
        $ostrich = new Ostrich();

        $this->assertEquals('insects, plants', $ostrich->eat());
        $this->assertEquals('chirp', $ostrich->tweet());

        $this->expectException(\InvalidArgumentException::class);
        $ostrich->fly();
    }
}
