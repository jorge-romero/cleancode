<?php

namespace Tests\Solid\LiskovSubstitution;

use PHPUnit\Framework\TestCase;
use Solid\LiskovSubstitution\Bird;
use Solid\LiskovSubstitution\Sparrow;

class SparrowTest extends TestCase
{
    public function testIsInstanceOfBird()
    {
        $sparrow = new Sparrow();

        $this->assertInstanceOf(Bird::class, $sparrow);
    }

    public function testDuck()
    {
        $sparrow = new Sparrow();

        $this->assertEquals('insect', $sparrow->eat());
        $this->assertEquals('chirp', $sparrow->tweet());
        $this->assertEquals('fly', $sparrow->fly());
    }
}
