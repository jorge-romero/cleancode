<?php

namespace Tests\Solid\LiskovSubstitution;

use PHPUnit\Framework\TestCase;
use Solid\LiskovSubstitution\Bird;
use Solid\LiskovSubstitution\Penguin;

class PenguinTest extends TestCase
{
    public function testIsInstanceOfBird()
    {
        $penguin = new Penguin();

        $this->assertInstanceOf(Bird::class, $penguin);
    }

    public function testPenguin()
    {
        $penguin = new Penguin();

        $this->assertEquals('fish', $penguin->eat());
        $this->assertEquals('squawk', $penguin->tweet());

        $this->expectException(\InvalidArgumentException::class);
        $penguin->fly();
    }
}
