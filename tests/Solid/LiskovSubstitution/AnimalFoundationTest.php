<?php

namespace Tests\Solid\LiskovSubstitution;

use PHPUnit\Framework\TestCase;
use Solid\LiskovSubstitution\AnimalFoundation;
use Solid\LiskovSubstitution\Bird;
use Solid\LiskovSubstitution\Duck;
use Solid\LiskovSubstitution\Ostrich;
use Solid\LiskovSubstitution\Penguin;
use Solid\LiskovSubstitution\Sparrow;

class AnimalFoundationTest extends TestCase
{
    public function testReleaseBirds()
    {
        $animalFoundation = new AnimalFoundation();

        $birds = [
            new Bird(),
            new Duck(),
            new Ostrich(),
            new Penguin(),
            new Sparrow(),
        ];

        $this->assertTrue($animalFoundation->releaseBirds($birds));
    }
}
