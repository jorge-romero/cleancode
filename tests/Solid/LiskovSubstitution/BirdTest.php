<?php

namespace Tests\Solid\LiskovSubstitution;

use PHPUnit\Framework\TestCase;
use Solid\LiskovSubstitution\Bird;

class BirdTest extends TestCase
{
    public function testBird()
    {
        $bird = new Bird();

        $this->assertEquals('eat', $bird->eat());
        $this->assertEquals('tweet', $bird->tweet());
        $this->assertEquals('fly', $bird->fly());
    }
}
