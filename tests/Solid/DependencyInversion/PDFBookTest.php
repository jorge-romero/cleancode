<?php

namespace Tests\Solid\DependencyInversion;

use PHPUnit\Framework\TestCase;
use Solid\DependencyInversion\PDFBook;

class PDFBookTest extends TestCase
{
    function testRead() 
    {
        $pdfBook = new PDFBook();
 
        $this->assertEquals('reading a pdf book', $pdfBook->read());
    }
}
