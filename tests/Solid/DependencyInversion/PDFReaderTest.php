<?php

namespace Tests\Solid\DependencyInversion;

use PHPUnit\Framework\TestCase;
use Solid\DependencyInversion\PDFBook;
use Solid\DependencyInversion\PDFReader;

class PDFReaderTest extends TestCase
{
    function testItCanReadAPDFBook() 
    {
        $pdfBook = new PDFBook();
        $pdfReader = new PDFReader($pdfBook);
 
        $this->assertRegExp('/pdf book/', $pdfReader->read());
    }
}
