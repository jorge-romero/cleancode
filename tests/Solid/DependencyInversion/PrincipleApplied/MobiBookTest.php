<?php

namespace Tests\Solid\DependencyInversion\PrincipleApplied;

use PHPUnit\Framework\TestCase;
use Solid\DependencyInversion\PrincipleApplied\MobiBook;

class MobiBookTest extends TestCase
{
    function testRead() 
    {
        $mobiBook = new MobiBook();
 
        $this->assertEquals('reading a mobi book', $mobiBook->read());
    }
}
