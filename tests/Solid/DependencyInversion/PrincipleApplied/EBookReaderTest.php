<?php

namespace Tests\Solid\DependencyInversion\PrincipleApplied;

use PHPUnit\Framework\TestCase;
use Solid\DependencyInversion\PrincipleApplied\EBookReader;
use Solid\DependencyInversion\PrincipleApplied\PDFBook;
use Solid\DependencyInversion\PrincipleApplied\MobiBook;

class EBookReaderTest extends TestCase
{
    function testItCanReadAPDFBook() 
    {
        $pdfBook = new PDFBook();
        $eBookReader = new EBookReader($pdfBook);
 
        $this->assertRegExp('/pdf book/', $eBookReader->read());
    }

    function testItCanReadAMobiBook() 
    {
        $mobiBook = new MobiBook();
        $eBookReader = new EBookReader($mobiBook);
 
        $this->assertRegExp('/mobi book/', $eBookReader->read());
    }
}
