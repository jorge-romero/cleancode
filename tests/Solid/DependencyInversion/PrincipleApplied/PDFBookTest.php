<?php

namespace Tests\Solid\DependencyInversion\PrincipleApplied;

use PHPUnit\Framework\TestCase;
use Solid\DependencyInversion\PrincipleApplied\PDFBook;

class PDFBookTest extends TestCase
{
    function testRead() 
    {
        $pdfBook = new PDFBook();
 
        $this->assertEquals('reading a pdf book', $pdfBook->read());
    }
}
