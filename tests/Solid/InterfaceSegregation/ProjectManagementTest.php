<?php

namespace Tests\Solid\InterfaceSegregation;

use PHPUnit\Framework\TestCase;
use Solid\InterfaceSegregation\Developer;
use Solid\InterfaceSegregation\ProjectManagement;
use Solid\InterfaceSegregation\Tester;

class ProjectManagementTest extends TestCase
{
    public function testProcessCodeUsingDeveloper()
    {
        $developer = new Developer();
        $projectManagement = new ProjectManagement();

        $this->assertTrue($projectManagement->processCode($developer));
    }

    public function testProcessCodeUsingTester()
    {
        $tester = new Tester();
        $projectManagement = new ProjectManagement();

        $this->assertFalse($projectManagement->processCode($tester));
    }
}
