<?php

namespace Tests\Solid\InterfaceSegregation\PrincipleApplied;

use PHPUnit\Framework\TestCase;
use Solid\InterfaceSegregation\PrincipleApplied\Programmer;

class ProgrammerTest extends TestCase
{
    public function testProgrammer()
    {
        $programmer = new Programmer();

        $this->assertEquals('coding', $programmer->code());
        $this->assertEquals('testing in localhost', $programmer->test());
    }
}
