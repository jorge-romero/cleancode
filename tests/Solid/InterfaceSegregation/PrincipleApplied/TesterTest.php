<?php

namespace Tests\Solid\InterfaceSegregation\PrincipleApplied;

use PHPUnit\Framework\TestCase;
use Solid\InterfaceSegregation\PrincipleApplied\Tester;

class TesterTest extends TestCase
{
    public function testTester()
    {
        $tester = new Tester();

        $this->assertEquals('testing in test server', $tester->test());
    }
}
