<?php

namespace Tests\Solid\InterfaceSegregation\PrincipleApplied;

use PHPUnit\Framework\TestCase;
use Solid\InterfaceSegregation\PrincipleApplied\Programmer;
use Solid\InterfaceSegregation\PrincipleApplied\ProjectManagement;

class ProjectManagementTest extends TestCase
{
    public function testProcessCodeUsingProgrammer()
    {
        $programmer = new Programmer();
        $projectManagement = new ProjectManagement();

        $this->assertTrue($projectManagement->processCode($programmer));
    }
}
