<?php

namespace Tests\Solid\InterfaceSegregation;

use PHPUnit\Framework\TestCase;
use Solid\InterfaceSegregation\Tester;

class TesterTest extends TestCase
{
    public function testTester()
    {
        $tester = new Tester();

        $this->assertFalse($tester->canCode());
        $this->assertEquals('testing in test server', $tester->test());

        $this->expectException(\InvalidArgumentException::class);
        $tester->code();
    }
}
