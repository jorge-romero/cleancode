<?php

namespace Tests\Solid\InterfaceSegregation;

use PHPUnit\Framework\TestCase;
use Solid\InterfaceSegregation\Developer;

class DeveloperTest extends TestCase
{
    public function testDeveloper()
    {
        $developer = new Developer();

        $this->assertTrue($developer->canCode());
        $this->assertEquals('coding', $developer->code());
        $this->assertEquals('testing in localhost', $developer->test());
    }
}
