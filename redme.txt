docker build -t phpunitdocker .
docker run -d -it --name phpunitdockerDev -p 8080:8080 --mount type=bind,source=/home/jromero/DockerProjects/phpunitdocker,target=/app phpunitdocker:latest
docker container start phpunitdockerDev
docker container stop phpunitdockerDev
docker container rm phpunitdockerDev
docker container exec -it phpunitdockerDev bash

[Excecute unit tests]
docker run -v /home/jromero/DockerProjects/phpunitdocker:/app --rm phpunit/phpunit tests --coverage-html coverage

docker container run -it -v $PWD:/app --name phpunitdockerDev --entrypoint "" phpunit/phpunit sh
docker container start phpunitdockerDev && docker container exec -it phpunitdockerDev bash
docker container stop phpunitdockerDev
phpunit tests --coverage-html coverage

docker run --rm -v $(pwd):/app composer/composer:latest require --dev phpunit/phpunit ^6.0
docker run -d -it --name phpunitdockerDev -p 8080:8080 --mount type=bind,source=/home/jromero/DockerProjects/phpunitdocker,target=/app composer:latest

docker run -v /home/jromero/DockerProjects/phpunitdocker:/app composer dump-autoload