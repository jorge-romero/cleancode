<?php

namespace Solid\InterfaceSegregation;

use Solid\InterfaceSegregation\Interfaces\IWorkable;

class Developer implements IWorkable
{
    public function canCode()
    {
        return true;
    }

    public function code()
    {
        return 'coding';
    }
    
    public function test()
    {
        return 'testing in localhost';
    }
}