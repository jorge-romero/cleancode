<?php

namespace Solid\InterfaceSegregation;

use Solid\InterfaceSegregation\Interfaces\IWorkable;

class Tester implements IWorkable
{
    public function canCode()
    {
        return false;
    }

    public function code()
    {
        throw new \InvalidArgumentException('Opps! I can not code');
    }

    public function test()
    {
        return 'testing in test server';
    }
}