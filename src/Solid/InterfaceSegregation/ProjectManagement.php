<?php

namespace Solid\InterfaceSegregation;

use Solid\InterfaceSegregation\Interfaces\IWorkable;

class ProjectManagement
{
    public function processCode(IWorkable $member)
    {
        $completeProcess = false;
        if ($member->canCode()) {
            $member->code();
            $completeProcess = true;
        }
        return $completeProcess;
    }
}
