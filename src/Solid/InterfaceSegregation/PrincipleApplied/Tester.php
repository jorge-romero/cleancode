<?php

namespace Solid\InterfaceSegregation\PrincipleApplied;

use Solid\InterfaceSegregation\PrincipleApplied\Interfaces\ITestable;

class Tester implements ITestable
{
    public function test()
    {
        return 'testing in test server';
    }
}