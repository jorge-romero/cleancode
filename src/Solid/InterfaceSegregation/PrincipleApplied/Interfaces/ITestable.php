<?php

namespace Solid\InterfaceSegregation\PrincipleApplied\Interfaces;

interface ITestable
{
    public function test();
}
