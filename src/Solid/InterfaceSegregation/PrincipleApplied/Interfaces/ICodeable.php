<?php

namespace Solid\InterfaceSegregation\PrincipleApplied\Interfaces;

interface ICodeable
{
    public function code();
}
