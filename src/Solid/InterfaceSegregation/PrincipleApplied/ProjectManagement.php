<?php

namespace Solid\InterfaceSegregation\PrincipleApplied;

use Solid\InterfaceSegregation\PrincipleApplied\Interfaces\ICodeable;

class ProjectManagement
{
    public function processCode(ICodeable $member)
    {
        $member->code();
        return true;
    }
}
