<?php

namespace Solid\InterfaceSegregation\PrincipleApplied;

use Solid\InterfaceSegregation\PrincipleApplied\Interfaces\ICodeable;
use Solid\InterfaceSegregation\PrincipleApplied\Interfaces\ITestable;

class Programmer implements ICodeable, ITestable
{
    public function code()
    {
        return 'coding';
    }

    public function test()
    {
        return 'testing in localhost';
    }
}