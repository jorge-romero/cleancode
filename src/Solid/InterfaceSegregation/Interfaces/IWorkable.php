<?php

namespace Solid\InterfaceSegregation\Interfaces;

interface IWorkable
{
    public function canCode();
    public function code();
    public function test();
}
