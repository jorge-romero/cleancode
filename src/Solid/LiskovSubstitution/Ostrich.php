<?php

namespace Solid\LiskovSubstitution;
use Solid\LiskovSubstitution\Bird;

class Ostrich extends Bird
{ 
    public function eat()
    {
        return 'insects, plants';
    }
 
    public function tweet()
    {
        return 'chirp';
    }

    public function fly()
    {
        throw new \InvalidArgumentException('Ostrich can\'t fly');
    }    
}