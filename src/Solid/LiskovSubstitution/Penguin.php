<?php

namespace Solid\LiskovSubstitution;
use Solid\LiskovSubstitution\Bird;

class Penguin extends Bird
{ 
    public function eat()
    {
        return 'fish';
    }
 
    public function tweet()
    {
        return 'squawk';
    }

    public function fly()
    {
        throw new \InvalidArgumentException('Penguin can\'t fly');
    }    
}