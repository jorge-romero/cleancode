<?php

namespace Solid\LiskovSubstitution;
use Solid\LiskovSubstitution\Bird;

class Sparrow extends Bird
{ 
    public function eat()
    {
        return 'insect';
    }
 
    public function tweet()
    {
        return 'chirp';
    }
}