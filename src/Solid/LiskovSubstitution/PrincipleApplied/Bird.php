<?php

namespace Solid\LiskovSubstitution\PrincipleApplied;

use Solid\LiskovSubstitution\PrincipleApplied\Interfaces\IMoveBird;

class Bird implements IMoveBird
{ 
    public function eat()
    {
        return 'eat';
    }
 
    public function tweet()
    {
        return 'tweet';
    }

    public function move()
    {
        return 'move';
    }
}