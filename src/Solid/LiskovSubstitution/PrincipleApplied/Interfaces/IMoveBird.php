<?php

namespace Solid\LiskovSubstitution\PrincipleApplied\Interfaces;

interface IMoveBird
{ 
    public function move();
}