<?php

namespace Solid\LiskovSubstitution\PrincipleApplied;

use Solid\LiskovSubstitution\PrincipleApplied\Bird;

class FlyingBird extends Bird
{
    public function move()
    {
        return 'fly';
    }
}