<?php

namespace Solid\LiskovSubstitution\PrincipleApplied;
use Solid\LiskovSubstitution\PrincipleApplied\RunnigBird;

class Ostrich extends RunnigBird
{ 
    public function eat()
    {
        return 'insects, plants';
    }
 
    public function tweet()
    {
        return 'chirp';
    }
}