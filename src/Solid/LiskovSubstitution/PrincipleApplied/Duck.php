<?php

namespace Solid\LiskovSubstitution\PrincipleApplied;

use Solid\LiskovSubstitution\PrincipleApplied\FlyingBird;

class Duck extends FlyingBird
{ 
    public function eat()
    {
        return 'fish';
    }
 
    public function tweet()
    {
        return 'quack';
    }
}