<?php

namespace Solid\LiskovSubstitution\PrincipleApplied;
use Solid\LiskovSubstitution\PrincipleApplied\SwimmingBird;

class Penguin extends SwimmingBird
{ 
    public function eat()
    {
        return 'fish';
    }
 
    public function tweet()
    {
        return 'squawk';
    }
}