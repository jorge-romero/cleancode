<?php

namespace Solid\LiskovSubstitution\PrincipleApplied;

use Solid\LiskovSubstitution\PrincipleApplied\Bird;

class AnimalFoundation 
{
    private function releaseBird(Bird $bird) {
        $bird->move();
    }

    public function releaseBirds(array $birds)
    {
        foreach($birds as $bird) {
            $this->releaseBird($bird);
        }
        return true;
    }
}