<?php

namespace Solid\LiskovSubstitution\PrincipleApplied;

class SwimmingBird extends Bird
{ 
    public function move()
    {
        return 'swim';
    }
}