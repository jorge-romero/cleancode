<?php

namespace Solid\LiskovSubstitution\PrincipleApplied;

class RunnigBird extends Bird
{ 
    public function move()
    {
        return 'run';
    }
}