<?php

namespace Solid\LiskovSubstitution\PrincipleApplied;
use Solid\LiskovSubstitution\PrincipleApplied\Bird;

class Sparrow extends FlyingBird
{ 
    public function eat()
    {
        return 'insect';
    }
 
    public function tweet()
    {
        return 'chirp';
    }
}