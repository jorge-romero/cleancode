<?php

namespace Solid\LiskovSubstitution;

use Solid\LiskovSubstitution\Bird;
use Solid\LiskovSubstitution\Ostrich;
use Solid\LiskovSubstitution\Penguin;

class AnimalFoundation 
{
    private function releaseBird(Bird $bird) {
        if (is_a($bird, Ostrich::class)) {
            // Ostrich can’t fly
        } else if (is_a($bird, Penguin::class)) {
            // Penguin can’t fly
        } else {
            $bird->fly();
        }
    }

    public function releaseBirds(array $birds)
    {
        foreach($birds as $bird) {
            $this->releaseBird($bird);
        }
        return true;
    }
}