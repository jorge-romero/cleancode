<?php

namespace Solid\LiskovSubstitution;
use Solid\LiskovSubstitution\Bird;

class Duck extends Bird
{ 
    public function eat()
    {
        return 'fish';
    }
 
    public function tweet()
    {
        return 'quack';
    }
}