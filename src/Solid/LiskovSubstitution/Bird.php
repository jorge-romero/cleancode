<?php

namespace Solid\LiskovSubstitution;

class Bird
{
    public function eat()
    {
        return 'eat';
    }
 
    public function tweet()
    {
        return 'tweet';
    }
 
    public function fly()
    {
        return 'fly';
    }
}