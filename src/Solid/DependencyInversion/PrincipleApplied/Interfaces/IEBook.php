<?php

namespace Solid\DependencyInversion\PrincipleApplied\Interfaces;

interface IEBook
{
    public function read();
}
