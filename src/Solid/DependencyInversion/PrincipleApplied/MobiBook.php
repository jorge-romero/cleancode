<?php

namespace Solid\DependencyInversion\PrincipleApplied;

use Solid\DependencyInversion\PrincipleApplied\Interfaces\IEBook;

class MobiBook implements IEBook
{ 
    function read() 
    {
        return 'reading a mobi book';
    }
}