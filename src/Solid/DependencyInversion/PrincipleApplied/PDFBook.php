<?php

namespace Solid\DependencyInversion\PrincipleApplied;

use Solid\DependencyInversion\PrincipleApplied\Interfaces\IEBook;

class PDFBook implements IEBook
{ 
    function read() 
    {
        return 'reading a pdf book';
    }
}