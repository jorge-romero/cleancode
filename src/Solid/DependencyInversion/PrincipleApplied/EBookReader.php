<?php

namespace Solid\DependencyInversion\PrincipleApplied;

use Solid\DependencyInversion\PrincipleApplied\EBookReader;
use Solid\DependencyInversion\PrincipleApplied\Interfaces\IEBook;

class EBookReader
{
    private $book;
 
    function __construct(IEBook $book) 
    {
        $this->book = $book;
    }
 
    function read() 
    {
        return $this->book->read();
    }
}