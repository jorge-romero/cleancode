<?php

namespace Solid\DependencyInversion;

use Solid\DependencyInversion\PDFBook;

class PDFReader
{
    private $book;
 
    function __construct(PDFBook $book) 
    {
        $this->book = $book;
    }
 
    function read() 
    {
        return $this->book->read();
    }
}