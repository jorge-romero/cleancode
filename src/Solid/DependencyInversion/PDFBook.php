<?php

namespace Solid\DependencyInversion;

class PDFBook
{ 
    function read() 
    {
        return 'reading a pdf book';
    }
}