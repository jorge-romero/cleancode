<?php

namespace Solid\OpenClose\PrincipleApplied;

class Customer
{
    private $normalPrice = 100;

    public function getNormalPrice() {
        return $this->normalPrice;
    }

    public function setNormalPrice($normalPrice) {
        $this->normalPrice = $normalPrice;
        return $this;
    }

    public function getMembershipPrice()
    {
        return $this->normalPrice;
    }
}
