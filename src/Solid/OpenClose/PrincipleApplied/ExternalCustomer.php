<?php

namespace Solid\OpenClose\PrincipleApplied;
use Solid\OpenClose\PrincipleApplied\Customer;

class ExternalCustomer extends Customer
{
    public function getMembershipPrice()
    {
        return $this->getNormalPrice() + ($this->getNormalPrice() * 0.2);
    }
}
