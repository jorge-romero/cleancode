<?php

namespace Solid\OpenClose;

class Customer
{
    const EXTERNAL = 2;
    const INTERNAL = 1;
    const PREFERENCE = 3;

    private $normalPrice = 100;

    public function getNormalPrice() {
        return $this->normalPrice;
    }

    public function setNormalPrice($normalPrice) {
        $this->normalPrice = $normalPrice;
        return $this;
    }

    public function getMembershipPrice($type = 0)
    {
        if ($type == self::INTERNAL) {
            return $this->getNormalPrice();
        }
        if ($type == self::EXTERNAL) {
            return $this->getNormalPrice() + ($this->getNormalPrice() * 0.2);
        }
        if ($type == self::PREFERENCE) {
            return $this->getNormalPrice() + ($this->getNormalPrice() * -0.1);
        }
        return $this->normalPrice;
    }
}
