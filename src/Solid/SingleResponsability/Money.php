<?php

namespace Solid\SingleResponsability;

class Money
{
    private $currency;
    private $value;

    public function __construct($currency, $value)
    {
        $this->currency = $currency;
        $this->value = $value;
    }

    public function getCurrency()
    {
        return $this->currency;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function toHtml()
    {
        return "<span><b>{$this->currency}</b>&nbsp;{$this->value}</span>";
    }

    public function toJson()
    {
        $data = [
            'currency' => $this->currency,
            'value' => $this->value,
        ];
        return json_encode($data);
    }
}
