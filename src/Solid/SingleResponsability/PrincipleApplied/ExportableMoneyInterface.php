<?php

namespace Solid\SingleResponsability\PrincipleApplied;
use Solid\SingleResponsability\PrincipleApplied\Money;

interface ExportableMoneyInterface
{
    public function export(Money $money);
}
