<?php

namespace Solid\SingleResponsability\PrincipleApplied;
use Solid\SingleResponsability\principleApplied\Money;
use Solid\SingleResponsability\PrincipleApplied\ExportableMoneyInterface;

class JsonExportableMoney implements ExportableMoneyInterface
{
    public function export(Money $money)
    {
        $data = [
            'currency' => $money->getCurrency(),
            'value' => $money->getValue(),
        ];
        return json_encode($data);
    }
}
