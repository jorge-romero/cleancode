<?php

namespace Solid\SingleResponsability\PrincipleApplied;
use Solid\SingleResponsability\principleApplied\Money;
use Solid\SingleResponsability\PrincipleApplied\ExportableMoneyInterface;

class HtmlExportableMoney implements ExportableMoneyInterface
{
    public function export(Money $money)
    {
        $currency = $money->getCurrency();
        $value = $money->getValue();

        return "<span><b>{$currency}</b>&nbsp;{$value}</span>";
    }
}
