<?php

namespace CleanCode\Exceptions;

class TemperatureHighException extends \Exception {
    public static function fromStation()
    {
        return new self('Temperature in the station is very high.');
    }
}
