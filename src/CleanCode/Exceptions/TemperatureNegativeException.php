<?php

namespace CleanCode\Exceptions;

class TemperatureNegativeException extends \Exception {
    public static function fromMeasure($measure)
    {
        return new self('Measure ' . $measure . ' should be positive.');
    }
}
