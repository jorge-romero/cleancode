<?php

namespace CleanCode;

use CleanCode\Exceptions\TemperatureHighException;
use CleanCode\Interfaces\IGetMeasureFromDB;
use CleanCode\Interfaces\INotification;
use CleanCode\Temperature;

class Station
{
    private $temperature;
    private $temperatureLimit = 80;

    private function __construct(IGetMeasureFromDB $iGetMeasureFromDB)
    {
        $measureFromDB = $iGetMeasureFromDB->getNormalLimitFromDB();
        $this->temperature = Temperature::take($measureFromDB);
    }

    public static function build(IGetMeasureFromDB $iGetMeasureFromDB)
    {
        return new static($iGetMeasureFromDB);
    }

    private function isTemperatureHigh() 
    {
        return ($this->temperature->measure() > $this->temperatureLimit);
    }

    private function sendNotification(INotification $iNotification, $message) {
        $iNotification->send($message);
    }

    public function checkTemperature(
        IGetMeasureFromDB $iGetMeasureFromDB, INotification $iNotification) 
    {
        if ($this->isTemperatureHigh()) {
            $this->sendNotification($iNotification, 'High temperature.');
            throw TemperatureHighException::fromStation();
        }
        return TRUE;
    }
}
