<?php

namespace CleanCode;

use CleanCode\Exceptions\TemperatureNegativeException;
use CleanCode\Interfaces\IGetMeasureFromDB;

class Temperature
{
    private $measure;

    private function __construct($measure)
    {
        $this->setMeasure($measure);
    }

    private function setMeasure($measure) {
        $this->checkMeasurePositive($measure);
        $this->measure = $measure;
    }

    private function checkMeasurePositive($measure) {
        if ($measure < 0) {
            throw TemperatureNegativeException::fromMeasure($measure);
        }
    }

    public function measure()
    {
        return $this->measure;
    }

    public static function take($measure)
    {
        return new static($measure);
    }
    
    public function isDangerous() 
    {
        $measureFromDB = $this->getMeasureFromDB();
        return ($this->measure() > $measureFromDB);
    }

    protected function getMeasureFromDB() 
    {
        $dbConnection = DBConnection::getConnection([
            'dbname' => 'temperature',
            'host' => 'localhost',
            'user' => 'admin',
            'password' => '123456',
        ]);

        return $dbConnection->getMeasureFromDB();
    }
    
    public function isNormal(IGetMeasureFromDB $iGetMeasureFromDB) 
    {
        $measureFromDB = $iGetMeasureFromDB->getNormalLimitFromDB();
        return ($this->measure() < $measureFromDB);
    }

    /**
        La ley de Demeter básicamente es un mecanismo de detección de acoplamiento, un objeto no debería conocer las entrañas de otros objetos con los que interactúa.

        Fuente: https://devexperto.com/ley-de-demeter/
     */
    public static function fromStation($station)
    {
        return new static(
            $station->sensor()->temperature()->measure()
        );
    }

    public function add(self $temperatureToAdd)
    {
        return new self($this->measure() + $temperatureToAdd->measure());
    }
}
