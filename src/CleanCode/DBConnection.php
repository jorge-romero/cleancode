<?php

namespace CleanCode;

class DBConnection
{
	private $defaultMeasure = 1;
	private $dataConnection = [];

    private function __construct($dataConnection)
    {
    	$this->dataConnection = $dataConnection;
    	unset($dataConnection);
    }

    public static function getConnection($dataConnection)
    {
        return new self($dataConnection);
    }

    public function getMeasureFromDB() 
    {
        return $this->defaultMeasure;
    }
}
