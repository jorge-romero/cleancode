<?php

namespace CleanCode\Interfaces;

interface IGetMeasureFromDB 
{
	public function getNormalLimitFromDB();
}
