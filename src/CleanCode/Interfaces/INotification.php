<?php

namespace CleanCode\Interfaces;

interface INotification
{
	public function send($message);
}
