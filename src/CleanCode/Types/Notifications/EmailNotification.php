<?php

namespace CleanCode\Types\Notifications;

use CleanCode\Interfaces\INotification;

class EmailNotification implements INotification
{
    private function __construct()
    {
    }

    public static function create()
    {
        return new static();
    }

    public function send($message) 
    {
        return 'Email sent!';
    }
}
