<?php

namespace CleanCode;

class TemperatureTestClass extends Temperature
{
    protected function getMeasureFromDB() 
    {
        return 80;
    }
}
