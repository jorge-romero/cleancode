<?php

namespace CleanCode\Services;

use CleanCode\Interfaces\IGetMeasureFromDB;
use CleanCode\Interfaces\INotification;
use CleanCode\Station;

class StationService
{
    private function __construct()
    {
    }

    public static function create()
    {
        return new static();
    }

    public function checkTemperature(
        IGetMeasureFromDB $iGetMeasureFromDB, INotification $iNotification)
    {
        $station = Station::build($iGetMeasureFromDB);
        return $station->checkTemperature($iGetMeasureFromDB, $iNotification);
    }
}
